# Test 1

## Dependencies

Compiling C code  
* `sudo apt install make`
* `sudo apt install gcc`

CUDA
* NVidia Graphics Card 
* [install CUDA](https://www.pugetsystems.com/labs/hpc/How-to-install-CUDA-9-2-on-Ubuntu-18-04-1184/)

Running programs

* Navigate to the question folder 

* Open terminal and run the bash file (.sh) with either `bash file.sh` or `./file.sh`

  

